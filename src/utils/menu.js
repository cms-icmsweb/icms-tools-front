import store from "@/store";

export default {
  collaboration: [
    {
      title: "Units",
      route: "units",
    },
    {
      title: "Institutes",
      route: "institutes",
    },
    { title: "People", route: "people" },
    {
      title: "Weekly Meetings",
      route: "weekly-meetings",
    },
    {
      title: "CMS Weeks",
      items: [
        {
          title: "Room Booking",
          route: "bookings",
        },
        { title: "CMS Week Management", route: "weeks" },
        {
          title: "Rooms",
          route: "rooms",
        },
      ],
    },
    {
      title: "Awards",
      items: [
        {
          title: "Overview",
          route: "awards",
        },
        {
          title: "Nomination Submission",
          route: "award-nomination-submission",
        },
        {
          title: "Nomination Review & Selection",
          route: "award-nomination-review-selection",
          guard: () => store.getters["user/isAwardAdmin"],
        },
        {
          title: "Awardees",
          route: "awardees",
          // route: "coming-soon",
        },
      ],
    },
    {
      title: "Job Openings",
      items: [
        { title: "Job Openings", route: "job-openings" },
        { title: "Job Nominations", route: "job-nominations" },
        {
          title: "Nominee Selection",
          route: "nominee-selection",
          // guard: function () {
          //   return (
          //     store.getters["user/isAdmin"] ||
          //     store.getters["user/isIcmsDev"] ||
          //     store.getters["user/isJobOpAdmin"]
          //   );
          // },
        },
        // { title: "Questionnaires", route: "questionnaires" },
      ],
    },
    { title: "Voting", route: "voting" },
    { title: "MO Lists", route: "mo-list" },
    { title: "Authorship Applications", route: "applicants" },
    { title: "Emeritus Nominations", route: "emeritus-nominations" },
    { title: "CMS Statistics", route: "statistics" },
    { title: "iCMS Flags", route: "flags" },
    { title: "PhD Info", route: "phd-info", guard: () => true },
    { title: "Overdue Graduations", route: "overdue-graduations" },
    { title: "Tenures", route: "tenures" },
    { title: "Communications", route: "communications" },
  ],

  institute: [
    { title: "Institute Profile", route: "institute-profile" },
    { title: "Pending Authors", route: "pending-authors" },
    { title: "Member Authors", route: "member-authors" },
    {
      title: "Vote Delegation",
      route: "vote-delegation",
      guard: function () {
        return (store.getters["user/ledInstitutes"] || []).length > 0;
      },
    },
    { title: "EPR Suspension", route: "suspensions" },
    { title: "Overdue Graduation", route: "overdue-graduations" },
  ],

  administration: [
    // Temporarily disable MoManagement until the page is finished
    // {
    //   title: "Secretariat",
    //   items: [{ title: "MO Management", route: "mo-management" }],
    // },
    {
      title: "Engagement Office",
      items: [
        {
          title: "EPR Due Corrector",
          route: "due-corrector",
        },
        {
          title: "EPR Due Corrections Log",
          route: "due-corrections",
        },
      ],
      guard: () => store.getters["user/isEngagementOffice"],
    },
    {
      title: "M&O",
      items: [{ title: "M&O Project List", route: "mo-project-list" }],
    },
  ],

  publications: [
    {
      title: "CADI Views",
      items: [
        { title: "CADI Lines", route: "cadi-lines" },
        { title: "ARC Members", route: "arc-members" },
        { title: "AWGs", route: "awgs" },
        {
          title: "Approved CADI Lines",
          route: "cadi-lines-approved",
        },
        {
          title: "Updated CADI Lines",
          route: "cadi-lines-updated",
        },
        {
          title: "PubComm Dashboard",
          route: "pub-comm-dashboard",
        },
        { title: "CADI Line Latex Build Checker", route: "check-latex-build" },
      ],
    },
    {
      title: "CMS Author Lists",
      items: [
        { title: "Show open Author Lists", route: "open-author-lists" },
        { title: "Browse AL Files", route: "author-lists-files" },
        { title: "Author Data Management", route: "author-data-management" },
      ],
    },
    {
      title: "CMS Notes",
      items: [
        { title: "Notes", route: "notes" },
        { title: "Workflows", route: "note-stubs" },
      ],
    },
  ],

  user: [
    { title: "Profile", route: "user-profile" },
    { title: "AL Mentions", route: "al-mentions" },
    {
      title: "Admin",
      items: [
        { title: "Emails", route: "email-list" },
        { title: "Compose Email", route: "compose-email" },
        { title: "Announcements", route: "announcements" },
        { title: "Permissions", route: "permissions" },
        { title: "CBI Flags Check", route: "cbi-flags" },
        {
          title: "Cross Check Applicant Dues",
          route: "applicant-dues",
        },
        { title: "Free MO Check", route: "free-authorship-mo" },
        { title: "Check DateEndSign", route: "date-end-sign" },
        {
          title: "Restplus API",
          url: process.env.VUE_APP_API_URL + "/restplus/",
        },
        { title: "Markdown Editor", route: "markdown-editor" },
        { title: "Text History", route: "text-history" },
        {
          title: "Award Types",
          route: "award-types",
        },
        { title: "Bulk MO Change", route: "coming-soon", guard: () => false },
        {
          title: "Import Missing Vote Delegations",
          route: "coming-soon",
          guard: () => false,
        },
        { title: "Playground", route: "coming-soon", guard: () => false },
      ],
    },
  ],
};
