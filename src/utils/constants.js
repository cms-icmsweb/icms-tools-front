export const icon = {
  account: "mdi-account",
  menu: "mdi-menu",
  seat: "mdi-seat",
  extension: "mdi-puzzle",
  edit: "mdi-pencil",
  add: "mdi-plus",
  add_circle: "mdi-plus-circle",
  add_person: "mdi-account-plus",
  select_person: "mdi-account-arrow-right",
  delete: "mdi-delete",
  search: "mdi-magnify",
  calendar: "mdi-calendar",
  save: "mdi-content-save",
  close: "mdi-close",
  list: "mdi-format-list-bulleted",
  incognito: "mdi-incognito",
  dots_vertical: "mdi-dots-vertical",
  accept: "mdi-check",
  check: "mdi-check-circle-outline",
  alert: "mdi-alert-circle-outline",
  info: "mdi-information-outline",

  down: "mdi-chevron-down",
  up: "mdi-chevron-up",
  left: "mdi-chevron-left",
  right: "mdi-chevron-right",
};

export const projects = [
  "",
  "BRIL",
  "CORE",
  "DAQ",
  "DI",
  "EC",
  "GEN",
  "HC",
  "HGCAL",
  "HGCAL (CE)",
  "L1TRG",
  "MTD",
  "MUCSC",
  "MUDT",
  "MUGEM",
  "Muon",
  "MURPC",
  "OFF",
  "OFFCOMP",
  "PPD",
  "PPS",
  "RUN",
  "TECH",
  "TK",
  "TRGCOORD",
  "UPGRADE",
];

export const personStatuses = [
  "CMS",
  "EXMEMBER",
  "NOSHOW",
  "NOTCMS",
  "NOTNAME",
  "RELATED",
  "CMSEXTENDED",
  "CMSEMERITUS",
  "CMSVO",
  "CMSASSOC",
  "DECEASED",
  "CMSAFFILIATE",
].sort();

//Flags
export const FLAG_MISC_AUTHORNO = "MISC_authorno";

// EXTERNAL URLS
export const LINK_ICMS_PRJLIST =
  "http://cms.cern.ch/iCMS/jsp/gen/admin/prjlist.jsp";

export const MAX_NUMBER_OF_PROJECTS = 2;
