import * as icmsPublic from "./endpoints/icms_public";
import * as orgChart from "./endpoints/org_chart";
import * as members from "./endpoints/members";
import * as voting from "./endpoints/voting";
import * as statistics from "./endpoints/statistics";
import * as authorship from "./endpoints/authorship";
import * as booking from "./endpoints/booking";
import * as admin from "./endpoints/admin";
import * as old_notes from "./endpoints/old_notes";
import * as inst from "./endpoints/inst";
import * as authorlist from "./endpoints/authorlist";
import * as mo from "./endpoints/mo";
import * as epr from "./endpoints/epr";
import * as general from "./endpoints/general";
import * as piggyback from "./endpoints/piggyback";
import * as janitorial from "./endpoints/janitorial";
import * as diagnostics from "./endpoints/diagnostics";
import * as cadi from "./endpoints/cadi";
import * as notes from "./endpoints/notes";
import * as awards from "./endpoints/awards";
import * as weekly_meetings from "./endpoints/weekly_meetings";
import * as phd_info from "./endpoints/phd_info";

export default {
  ...icmsPublic,
  ...orgChart,
  ...members,
  ...voting,
  ...statistics,
  ...authorship,
  ...booking,
  ...admin,
  ...old_notes,
  ...inst,
  ...authorlist,
  ...epr,
  ...mo,
  ...general,
  ...piggyback,
  ...janitorial,
  ...diagnostics,
  ...cadi,
  ...notes,
  ...awards,
  ...weekly_meetings,
  ...phd_info,
};
