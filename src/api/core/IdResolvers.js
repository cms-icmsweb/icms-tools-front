import { IdResolver } from "common";
import api from "@/api";

export class CmsIdResolver extends IdResolver {
  constructor(parentComponent) {
    super(parentComponent, "cmsId");
    this.apiCall = new api.PeopleCall().setSuccessCallback((data) =>
      this.storeRetrieved(data)
    );
  }
  fetchMissing() {
    this.apiCall.setCmsIds(this.getIdsToResolve()).get();
  }
  getLabelForId(cmsId) {
    var data = this.getDataForId(cmsId);
    if (data !== null && data !== undefined) {
      return data.firstName + " " + data.lastName;
    }
    return cmsId;
  }
}
