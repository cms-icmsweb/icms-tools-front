import { RestplusCall } from "@/api/core";

export class WeeklyMeetingsCall extends RestplusCall {
  constructor() {
    super("/weekly_meetings");
  }

  setTitle(value) {
    return this.set("title", value);
  }
  setOddEvenWeekly(value) {
    return this.set("odd_even_weekly", value);
  }
  setDayOfWeek(value) {
    return this.set("day_of_week", value);
  }
  setStartTime(value) {
    return this.set("start_time", value);
  }
  setEndTime(value) {
    return this.set("end_time", value);
  }
  setRoom(value) {
    return this.set("room", value);
  }
  setPrimaryContact(value) {
    return this.set("primary_contact", value);
  }
  setState(value) {
    return this.set("state", value);
  }
  setConveners(value) {
    return this.set("weekly_meeting_conveners", value);
  }
}