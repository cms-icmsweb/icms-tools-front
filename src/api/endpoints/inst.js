import { RestplusCall } from "@/api/core";

export class OverdueGraduationsCall extends RestplusCall {
  constructor() {
    super("/inst/overdue-graduations");
  }
}

export class ConfirmStudentStatusCall extends RestplusCall {
  constructor() {
    super("/inst/overdue-graduations/status");
  }
  setCmsId(value) {
    return this.set("cmsId", value);
  }
  setStatus(value) {
    return this.set("status", value);
  }
}

export class SuspensionsCall extends RestplusCall {
  constructor() {
    super("/inst/suspensions");
  }
  setCmsId(value) {
    return this.set("cmsId", value);
  }
  setIsAuthorSuspended(value) {
    return this.set("isAuthorSuspended", value);
  }
}
