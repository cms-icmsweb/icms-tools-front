import { RestplusCall, CrudCall } from "@/api/core/BaseCalls";

export class VotingParticipantsCall extends RestplusCall {
  constructor() {
    super("/voting/participants");
  }

  setCode(value) {
    return this.set("code", value);
  }

  setIncludeNearlyVoting(value) {
    return this.set("include_nearly_voting", value);
  }
}

export class VotingListsCall extends RestplusCall {
  constructor() {
    super("/voting/lists");
  }
  setCode(value) {
    return this.set("event_code", value);
  }
}

export class VotingEventsCall extends CrudCall {
  constructor() {
    super("/voting/events");
  }

  setIdInBody(value) {
    return this.set("id", value);
  }

  setCode(value) {
    return this.set("code", value);
  }

  setType(value) {
    return this.set("type", value);
  }

  setTitle(value) {
    return this.set("title", value);
  }

  setListClosingDate(value) {
    return this.set("list_closting_date", value);
  }

  setStartTime(value) {
    return this.set("start_time", value);
  }

  setEndTime(value) {
    return this.set("end_time", value);
  }

  setDelegationDeadline(value) {
    return this.set("delegation_deadline", value);
  }

  setApplicableMoYear(value) {
    return this.set("applicable_mo_year", value);
  }

  setDelegationAllowed(value) {
    return this.set("is_delegation_allowed", value);
  }
}

export class VoteDelegationsCall extends CrudCall {
  constructor() {
    super("/voting/delegations");
  }

  setPrincipalCmsId(value) {
    return this.set("principal_cms_id", value);
  }

  setVotingCode(value) {
    return this.set("voting_code", value);
  }

  setDelegateCmsId(value) {
    return this.set("delegate_cms_id", value);
  }

  setInstCode(value) {
    return this.set("inst_code", value);
  }

  setPerennial() {
    return this.set("is_perennial", true);
  }

  setCancelled() {
    return this.set("status", "cancelled");
  }
}
