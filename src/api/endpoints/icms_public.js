import { RestplusCall } from "@/api/core";

export class InstSuggestionsCall extends RestplusCall {
  constructor(searchTerm) {
    super("/icms_public/autocomplete_institutes/" + searchTerm);
  }
}

export class PeopleSuggestionsCall extends RestplusCall {
  constructor(searchTerm) {
    super("/icms_public/autocomplete_people/" + searchTerm);
  }

  setTeamOnly(value) {
    return this.set("team_only", value);
  }
}

export class GrappaEgroupsCall extends RestplusCall {
  constructor() {
    super("/icms_public/grappa_egroups");
  }

  setCmsId(value) {
    return this.set("cms_id", value);
  }
}

export class UserInfoCall extends RestplusCall {
  constructor() {
    super("/icms_public/user_info");
  }

  setCmsId(value) {
    return this.set("cms_id", value);
  }
}

export class ParsedHistoryLinesCall extends RestplusCall {
  constructor() {
    super("/icms_public/parsed_history");
  }

  setCmsId(value) {
    return this.set("cms_id", value);
  }
}

export class ActiveAnnouncements extends RestplusCall {
  constructor() {
    super("/icms_public/announcements");
    this.set("active_only", true);
  }
}

export class CMSProjects extends RestplusCall {
  constructor() {
    super("/icms_public/cms_projects");
    this.set("official", true);
    this.set("status", "active");
  }
}
