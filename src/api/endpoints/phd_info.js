import { RestplusCall } from "@/api/core";

export class PhdInfoCall extends RestplusCall {
  constructor() {
    super("/phd_info");
  }

  setThesisTitle(value) {
    return this.set("thesis_title", value);
  }
  setThesisLink(value) {
    return this.set("thesis_link", value);
  }
  setStudent(value) {
    return this.set("student", value);
  }
  setStudentInstitute(value) {
    return this.set("student_institute", value);
  }
  setEnrollmentDate(value) {
    return this.set("enrollment_date", value);
  }
  setDissertationDefenseDate(value) {
    return this.set("dissertation_defense_date", value);
  }
  setPhysicsGroups(value) {
    return this.set("physics_groups", value);
  }
  setCmsProjects(value) {
    return this.set("cms_projects", value);
  }
  setCadiAnalyses(value) {
    return this.set("cadi_analyses", value);
  }
  setCmsNotes(value) {
    return this.set("cms_notes", value);
  }
  setRemarks(value) {
    return this.set("remarks", value);
  }
  setIsActive(value) {
    return this.set("is_active", value);
  }
  setUpdater(value) {
    return this.set("updater", value);
  }
}
