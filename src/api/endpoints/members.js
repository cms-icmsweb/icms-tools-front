import { RestplusCall, restplusApi } from "@/api/core";

export const getPeopleData = ({
  statuses = ["CMS"],
  projects = [],
  authorFlags = [true],
}) => {
  let url = "/members/people?";
  (statuses || []).forEach((e) => {
    url += "ind_status=" + e + "&";
  });
  (projects || []).forEach((e) => {
    url += "project=" + e + "&";
  });
  (authorFlags || []).forEach((e) => {
    url += "is_author=" + e + "&";
  });
  return restplusApi.get(url);
};

export const getInstitutes = ({ codes = [], statuses = [] }) => {
  var url = "/members/institutes?";
  codes.forEach(function (code) {
    url += "code=" + code + "&";
  });
  statuses.forEach(function (status) {
    url += "status=" + status + "&";
  });

  return restplusApi.get(url);
};

export class UsersLiteLdapInfoCall extends RestplusCall {
  constructor() {
    super("/members/ldap_people_lite_info");
    this.set("gid", 1399);
  }
}

export class ExternalOutreachContactsCall extends RestplusCall {
  constructor() {
    super("/members/external_outreach_contacts");
  }
  setCode(value) {
    return this.set("code", value);
  }
  setType(value) {
    return this.set("type", value);
  }
  setTitle(value) {
    return this.set("title", value);
  }
  setFirstName(value) {
    return this.set("firstname", value);
  }
  setLastName(value) {
    return this.set("lastname", value);
  }
  setEmail(value) {
    return this.set("email", value);
  }
}

export class UsersInfoCall extends RestplusCall {
  constructor() {
    super("/members/users_info");
    console.debug("Users info call constructed");
  }
  setCmsId(value) {
    return this.set("cms_id", value);
  }
  setInstCode(value) {
    return this.set("inst_code", value);
  }
  setStatus(value) {
    return this.set("status", value);
  }
}

export class AssignmentsCall extends RestplusCall {
  constructor() {
    super("/members/assignments");
  }
  setCmsId(value) {
    return this.set("cms_id", value);
  }
  setId(value) {
    return this.set("id", value);
  }
  setProjectCode(value) {
    return this.set("project_code", value);
  }
  setFraction(value) {
    return this.set("fraction", value);
  }
  setActiveOnly(value) {
    return this.set("active_only", value);
  }
  setAssignmentsList(value) { 
    return this.set("assignments", value);
  }
}

export class InstitutesCall extends RestplusCall {
  constructor() {
    super("/members/institutes");
    this.set("code", []);
    this.set("status", []);
  }
  addCode(value) {
    this.data["code"].push(value);
    return this;
  }
  addStatus(value) {
    this.data["status"].push(value);
    return this;
  }
}

export class MemberPeopleCall extends RestplusCall {
  constructor() {
    super("/members/people");
  }
  setInstCode(value) {
    return this.set("inst_code", value);
  }
  setProject(value) {
    return this.set("project", value);
  }
  setStatus(value) {
    return this.set("ind_status", value);
  }
  setIsAuthor(value) {
    return this.set("is_author", value);
  }
}

export class InstInfoCall extends RestplusCall {
  constructor() {
    super("/members/institute");
  }
  setCode(value) {
    return this.set("code", value);
  }
}

export class OldDbPersonInfoCall extends RestplusCall {
  constructor() {
    super("/members/person");
  }
  setCmsId(value) {
    return this.set("cms_id", value);
  }
}

export class RestrictedPersonInfoCall extends RestplusCall {
  constructor() {
    super("/members/person_restricted_info");
  }
  setCmsId(value) {
    return this.set("cms_id", value);
  }
}

export class CareerEventsCall extends RestplusCall {
  constructor() {
    super("/members/career_events");
  }
  setCmsId(value) {
    return this.set("cms_id", value);
  }
  setEventType(value) {
    return this.set("event_type", value);
  }
}

export class CareerEventCall extends RestplusCall {
  constructor() {
    super("/members/career_event");
  }

  setId(value) {
    return this.set("id", value);
  }
  setCmsId(value) {
    return this.set("cms_id", value);
  }
  setDate(value) {
    return this.set("date", value);
  }
  setEventType(value) {
    return this.set("event_type", value);
  }
}

export class UserLdapInfoCall extends RestplusCall {
  constructor() {
    super("/members/ldap_person_info");
  }
  setHrId(value) {
    return this.set("hr_id", value);
  }
}

export class FlagsCall extends RestplusCall {
  constructor() {
    super("/members/flags");
  }
}

export class EgroupInfo extends RestplusCall {
  constructor() {
    super("/members/egroup-info");
  }

  setHrId(value) {
    return this.set("hr_id", value);
  }
}
