import { RestplusCall, IcmsPiggybackCall } from "@/api/core";

export class CadiLinesCall extends IcmsPiggybackCall {
  constructor() {
    super("/cadi/analyses");
  }
  setAwg(value) {
    return this.set("awg", value);
  }
  setYear(value) {
    return this.set("year", value);
  }
  setCodes(values) {
    return this.set("codes", values.join());
  }
}

export class CadiLineUpdatesCall extends IcmsPiggybackCall {
  constructor() {
    super("/cadi/history/updates");
  }
  setMinDate(value) {
    return this.set("minDate", value);
  }
  setMaxDate(value) {
    return this.set("maxDate", value);
  }
}

export class CadiLineStateChangesCall extends IcmsPiggybackCall {
  constructor() {
    super("/cadi/history/stateChanges");
  }
  setMinDate(value) {
    return this.set("minDate", value);
  }
  setMaxDate(value) {
    return this.set("maxDate", value);
  }
  setTargetState(value) {
    return this.set("targetStates", value);
  }
  /**
   * Set multiple target states
   * @param {Array} values A list of states
   */
  setTargetStates(values) {
    return this.setTargetState(values.join());
  }
}

export class ToolkitCadiLineStateChangesCall extends RestplusCall {
  constructor() {
    super("/cadi/updated-lines");
  }
  setMinDate(value) {
    return this.set("start_date", value);
  }
  setMaxDate(value) {
    return this.set("end_date", value);
  }
  setYear(value) {
    return this.set("year", value);
  }
  setAwg(value) {
    return this.set("awg", value);
  }
  /**
   * Set multiple target states
   * @param {Array} values A list of states
   */
  setTargetStates(values) {
    return this.set("to_status", values.join(","));
  }
  setCurrentStates(values) {
    if (values === undefined) {
      return this;
    }
    return this.set("current_status", values.join(","));
  }
}

export class ArcsCall extends IcmsPiggybackCall {
  constructor() {
    super("/cadi/arcs");
  }
}

export class AwgsCall extends IcmsPiggybackCall {
  constructor() {
    super("/cadi/awgs");
  }
}

export class CheckLatexBuildCall extends RestplusCall {
  constructor() {
    super("/cadi/check-latex-build");
  }
  setCadiLine(value) {
    return this.set("cadiLine", value);
  }
  setCadiType(value) {
    return this.set("cadiType", value);
  }
  setDataMC(value) {
    return this.set("dataMC", value);
  }
  setPreview(value) {
    return this.set("preview", value);
  }
  setVerbose(value) {
    return this.set("verbose", value);
  }
  setKeepFiles(value) {
    return this.set("keepFiles", value);
  }
}

/**
 * This one fetches a mapping between CADI lines and CDS record numbers.
 * Data itself is pre-fetched and stored in toolkit's DB.
 * Somewhere along the way it also checks the json data file used for the CMS results page.
 */
export class PaperExternalDataCall extends RestplusCall {
  constructor() {
    super("/cadi/paper_external_data");
  }
}

export class XebReportCall extends RestplusCall {
  constructor() {
    super("/cadi/xeb_report");
  }
  setLookbackPeriod(days) {
    return this.set("xeb_report_period", days);
  }
}

export class CadiAnalysisSuggestionsCall extends RestplusCall {
  constructor(searchTerm) {
    super("/cadi/autocomplete_analysis_codes");
    this.set("search_term", searchTerm);
    this.set("include_inactive", false);
  }
}

export class CadiGroups extends RestplusCall {
  constructor() {
    super("/cadi/cadi_group_names");
    this.set("active_only", true);
  }
}
