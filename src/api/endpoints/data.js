import { RestplusCall } from "@/api/core";

export class FlagsCall extends RestplusCall {
  constructor() {
    super("/data/flags");
  }
}
