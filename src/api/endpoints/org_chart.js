import { RestplusCall, restplusNamespace } from "@/api/core";
import { trimJson, expandParametersIntoUrl } from "@/utils/helpers";

const restplus = restplusNamespace("/org_chart");

export const getTenures = ({
  cms_id = [],
  as_of,
  unit_type = [],
  domain = [],
  unit_id = [],
}) => {
  const cmsIds = Array.isArray(cms_id) ? cms_id : [cms_id];
  const unit_types = Array.isArray(unit_type) ? unit_type : [unit_type];
  const domains = Array.isArray(domain) ? domain : [domain];
  const unit_ids = Array.isArray(unit_id) ? unit_id : [unit_id];

  let url = "/tenures?";
  cmsIds.forEach(function (val) {
    url += "cms_id=" + val + "&";
  });
  unit_types.forEach(function (val) {
    url += "unit_type=" + val + "&";
  });
  domains.forEach(function (val) {
    url += "domain=" + val + "&";
  });
  unit_ids.forEach(function (val) {
    url += "unit_id=" + val + "&";
  });

  return restplus.get(url, trimJson({ exclude_past: false, as_of }));
};

export const getPositions = ({
  ids = [],
  names = [],
  unit_types = [],
  unit_ids = [],
}) => {
  let url = "/positions?";
  ids.forEach(function (val) {
    url += "id=" + val + "&";
  });
  names.forEach(function (val) {
    url += "name=" + val + "&";
  });
  unit_types.forEach(function (val) {
    url += "unit_type=" + val + "&";
  });
  unit_ids.forEach(function (val) {
    url += "unit_id=" + val + "&";
  });

  restplus.get(url);
};

export const getTenure = (id) => restplus.get("/tenure", { id });

export const postTenure = ({
  id,
  cms_id,
  unit_id,
  position_id,
  start_date,
  end_date,
  designate = false,
}) =>
  restplus.post(designate === true ? "/designations" : "/tenures", {
    id,
    cms_id,
    position_id,
    unit_id,
    start_date,
    end_date,
  });

export const deleteDesignation = (id) =>
  restplus.delete("/designations", { id });

export const postDesignation = ({
  cmsId = null,
  unitId = null,
  positionId = null,
  startDate = null,
  endDate = null,
}) => {
  startDate =
    startDate != null ? startDate : new Date().toISOString().substring(0, 10);
  postTenure({
    id: null,
    cmsId,
    unitId,
    positionId,
    startDate,
    endDate,
    designate: true,
  });
};

export const deleteTenure = (id) => restplus.delete("/tenures", { id });

export class OrgChartPositionsCall extends RestplusCall {
  constructor() {
    super("/org_chart/positions");
  }

  setId(value) {
    return this.set("id", value);
  }
  setUnitId(value) {
    return this.set("unit_id", value);
  }
  setName(value) {
    return this.set("name", value);
  }
  setUnitTypeId(value) {
    return this.set("unit_type_id", value);
  }
  setLevel(value) {
    return this.set("level", value);
  }
  setIsActive(value) {
    return this.set("is_active", value);
  }
}

export class ExOfficioMandateCall extends RestplusCall {
  constructor() {
    super("/org_chart/ex_officio_mandate");
  }
  setId(value) {
    return this.set("id", value);
  }
}

export class ExOfficioMandatesCall extends RestplusCall {
  constructor() {
    super("/org_chart/ex_officio_mandates");
  }
  setId(value) {
    return this.set("id", value);
  }
  setStartDate(value) {
    return this.set("start_date", value);
  }
  setEndDate(value) {
    return this.set("end_date", value);
  }
  setSrcUnitId(value) {
    return this.set("src_unit_id", value);
  }
  setDstUnitId(value) {
    return this.set("dst_unit_id", value);
  }
  setSrcPositionLevel(value) {
    return this.set("src_position_level", value);
  }
  setDstPositionLevel(value) {
    return this.set("dst_position_level", value);
  }
  setSrcPositionName(value) {
    return this.set("src_position_name", value);
  }
  setDstPositionName(value) {
    return this.set("dst_position_name", value);
  }
}

export class OrgUnitsCall extends RestplusCall {
  constructor() {
    super("/org_chart/units");
  }
  setId(value) {
    return this.set("id", value);
  }
  setType(value) {
    return this.set("type", value);
  }
  setDomain(value) {
    return this.set("domain", value);
  }
  setActive(value) {
    return this.set("is_active", value);
  }
  setMinUnitLevel(value) {
    return this.set("min_unit_level", value);
  }
  setMaxUnitLevel(value) {
    return this.set("max_unit_level", value);
  }
  setSuperiorUnitId(value) {
    return this.set("superior_unit_id", value);
  }
  setEnclosingUnitId(value) {
    return this.set("eclosing_unit_id", value);
  }
  setOutermostUnitId(value) {
    return this.set("outermost_unit_id", value);
  }
}

export class OrgUnitCall extends RestplusCall {
  constructor() {
    super("/org_chart/unit");
  }

  setId(value) {
    return this.set("id", value);
  }
}

export class OrgUnitTypesCall extends RestplusCall {
  constructor() {
    super("/org_chart/unit_types");
  }

  setId(value) {
    return this.set("id", value);
  }

  setName(value) {
    return this.set("name", value);
  }

  setLevel(value) {
    return this.set("level", value);
  }
}

export class TenuresCall extends RestplusCall {
  constructor() {
    super("/org_chart/tenures");
  }
  setCmsId(value) {
    return this.set("cms_id", value);
  }
  setExcludePast(value) {
    return this.set("exclude_past", value);
  }
  setAsOf(value) {
    return this.set("as_of", value);
  }
  setDomain(value) {
    return this.set("domain", value);
  }
  setUnitType(value) {
    return this.set("unit_type", value);
  }
  setUnitId(value) {
    return this.set("unit_id", value);
  }
  setMinPositionLevel(value) {
    return this.set("min_position_level", value);
  }
  setMaxPositionLevel(value) {
    return this.set("max_position_level", value);
  }
}

export class JobOpeningsCall extends RestplusCall {
  constructor() {
    super("/org_chart/job_openings");
  }
  // setId(value) {
  //   return this.set("id", value);
  // }
  setTitle(value) {
    return this.set("title", value);
  }
  setPositionId(value) {
    return this.set("position_id", value);
  }
  setPositions(value) {
    return this.set("positions", value);
  }
  setDescription(value) {
    return this.set("description", value);
  }
  setRequirements(value) {
    return this.set("requirements", value);
  }
  setStartDate(value) {
    return this.set("start_date", value);
  }
  setEndDate(value) {
    return this.set("end_date", value);
  }
  setDeadline(value) {
    return this.set("nominations_deadline", value);
  }
  setStatus(value) {
    return this.set("status", value);
  }
  setComment(value) {
    return this.set("comment", value);
  }
  setNotifyEgroups(value) {
    return this.set("notify_egroups", value);
  }
  setDaysForNomineeResponse(value) {
    return this.set("days_for_nominee_response", value);
  }
  setJobUnitId(value) {
    return this.set("job_unit_id", value);
  }
  setNomineeNoReply(value) {
    return this.set("nominee_no_reply", value);
  }
  setSendMailToNominee(value) {
    return this.set("send_mail_to_nominee", value);
  }
  setQuestionnaireId(value) {
    return this.set("questionnaire_id", value);
  }
  setQuestionnaireUri(value) {
    return this.set("questionnaire_uri", value);
  }
}

export class JobNominationCall extends RestplusCall {
  constructor(for_nominee_selection = false) {
    super(
      expandParametersIntoUrl(
        "/org_chart/job_nomination?",
        trimJson({
          for_nominee_selection: for_nominee_selection,
        })
      )
    );
  }

  setJobId(value) {
    return this.set("job_id", value);
  }
  setNomineeId(value) {
    return this.set("nominee_id", value);
  }
  setRemarks(value) {
    return this.set("actor_remarks", value);
  }
  setJobUnitId(value) {
    return this.set("job_unit_id", value);
  }
  setPositionId(value) {
    return this.set("position_id", value);
  }
}

export class JobNominationStatusCall extends RestplusCall {
  constructor() {
    super("/org_chart/job_nomination");
  }
  setActorRemarks(value) {
    return this.set("actor_remarks", value);
  }
  setActorId(value) {
    return this.set("actor_id", value);
  }
  setStatus(value) {
    return this.set("status", value);
  }
  setAnswers(value) {
    return this.set("questionnaire_answers", value);
  }
}

export class JobQuestionnairesCall extends RestplusCall {
  constructor() {
    super("/org_chart/job_questionnaire");
  }
  setTitle(value) {
    return this.set("title", value);
  }
  setQuestions(value) {
    return this.set("questions", value);
  }
}
