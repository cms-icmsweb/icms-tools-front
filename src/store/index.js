import Vue from "vue";
import Vuex from "vuex";

import user from "./modules/user";
import grappaEgroups from "./modules/grappaEgroups";
import { notificationModule } from "common";

Vue.use(Vuex);

const storeConfig = {
  modules: {
    user,
    grappaEgroups,
    notification: notificationModule,
  },
};

export default new Vuex.Store(storeConfig);
export const createStore = () => new Vuex.Store(storeConfig);
