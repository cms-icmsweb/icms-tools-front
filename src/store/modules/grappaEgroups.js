import api from "@/api";

const state = {
  data: null,
  timestamp: null,
};

const mutations = {
  SET_GRAPPA_EGROUPS(state, payload) {
    state.data = payload;
    state.timestamp = Date.now();
  },
};

const actions = {
  fetchGrappaEgroups({ commit, state }) {
    // Check if data is expired (1 hour = 3600000 ms)
    const minutes30 = 1800000;
    if (
      state.data &&
      state.timestamp &&
      Date.now() - state.timestamp < minutes30
    ) {
      return Promise.resolve(state.data); // Return cached data if not expired
    }

    // If expired or not set, fetch from API
    return new api.GrappaEgroupsCall().get().then((data) => {
      commit("SET_GRAPPA_EGROUPS", data);
      return data;
    });
  },
};

const getters = {
  grappaEgroups: (state) => state.data,
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
