import api from "@/api";
import moment from "moment";

const state = () => ({
  data: {},
});

const getters = {
  hrId: (state) => state.data.hr_id,
  userId: (state) => state.data.cms_id,
  firstName: (state) => state.data.first_name,
  lastName: (state) => state.data.last_name,
  username: (state) => state.data.login,
  userInstCode: (state) => state.data.inst_code,
  userTenures: (state) => state.data.tenures || [],
  userFlags: (state) => state.data.flags,
  ledInstitutes: (state) =>
    Object.entries(state.data.team_duties || {})
      .filter((e) => e[1] == "leader")
      .map((e) => e[0]),
  isRoot: (state) => {
    const { flags } = state.data;
    return flags != null && flags != "undefined" && flags.includes("ICMS_root");
  },
  isAdmin: (state, getters) => {
    const { flags } = state.data;
    return (
      getters.isRoot ||
      (flags != null && flags != "undefined" && flags.includes("ICMS_admin"))
    );
  },
  // egroup name is a placeholder replace with true name when the group is created
  isDiversityOfficeChair: (state) =>
    state.data.cms_egroups &&
    state.data.cms_egroups.includes("cms-diversity-office-chair"),
  // isJobOpAdmin: (state) =>
  //   state.data.grappa_egroups &&
  //   state.data.grappa_egroups.includes("cms-jobop-admins"),
  isCbChairTeam: (state) =>
    state.data.cms_egroups &&
    state.data.cms_egroups.includes("cms-cbchairteam"),
  isIcmsDev: (state) =>
    state.data.cms_egroups && state.data.cms_egroups.includes("icms-devs"),
  isAwardAdmin: (state) =>
    state.data.cms_egroups &&
    state.data.cms_egroups.includes("cms-award-admins"),
  isImpersonating: (state) =>
    state.data.impersonation &&
    state.data.impersonation.real_cms_id !==
      state.data.impersonation.active_cms_id,
  isEngagementOffice: (state) =>
    state.data.flags && state.data.flags.includes("ENGT_OFFICE"),
  isSecretariat: (state) =>
    state.data.flags && state.data.flags.includes("ICMS_secr"),
  currentUser: (state, getters) => {
    return `${getters.firstName} ${getters.lastName} (${getters.userId})`;
  },
  // grappaEgroups: (state) => state.data.grappa_egroups,
};

const mutations = {
  setData(state, data) {
    state.data = { ...data, impersonation: {} };
  },
  setTenures(state, data) {
    state.data.tenures = data;
  },
  setIsImpersonation(state, data) {
    state.data.impersonation = data;
  },
};

const actions = {
  async fetchUser({ dispatch, commit }) {
    const data = await new api.UserInfoCall().get();
    commit("setData", data);
    dispatch("fetchTenures", data.cms_id);
    dispatch("fetchIsImpersonating");
  },

  async fetchTenures({ commit }, cms_id) {
    const data = await api.getTenures({
      cms_id,
      exclude_past: false,
      as_of: moment().format("YYYY-MM-DD"),
    });
    commit("setTenures", data);
  },

  async fetchIsImpersonating({ commit }) {
    const data = await new api.ImpersonateUserCall().get();
    commit("setIsImpersonation", data);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
