import DataFlags from "@/components/pages/collaboration/DataFlags";
import OrgUnits from "@/components/pages/collaboration/OrgUnits";
import OrgPositions from "@/components/pages/collaboration/OrgUnits/OrgPositions";
import OrgInstitutes from "@/components/pages/collaboration/OrgInstitutes";
import OrgPeople from "@/components/pages/collaboration/OrgPeople";
import CmsBookings from "@/components/pages/collaboration/cms_weeks/CmsBookings";
import CmsWeeks from "@/components/pages/collaboration/cms_weeks/CmsWeeks";
import CmsRooms from "@/components/pages/collaboration/cms_weeks/CmsRooms";
import VotingList from "@/components/pages/collaboration/VotingList";
import MoList from "@/components/pages/collaboration/MoList";
import AuthorApplicants from "@/components/pages/collaboration/AuthorAppicants";
import EmeritusNominations from "@/components/pages/collaboration/EmeritusNominations";
import MemberStatistics from "@/components/pages/collaboration/MemberStatistics";
import CmsTenures from "@/components/pages/collaboration/CmsTenures";
import CommunicationsOverview from "@/components/pages/collaboration/CommunicationsOverview";
import ExOfficioMandates from "@/components/pages/collaboration/OrgUnits/ExOfficioMandates.vue";
import Awardees from "@/components/pages/collaboration/awards/Awardees";
import Awards from "@/components/pages/collaboration/awards/Awards";
import AwardNominationReviewSelection from "@/components/pages/collaboration/awards/AwardNominationReviewSelection";
import AwardNominations from "@/components/pages/collaboration/awards/AwardNominations";
import JobOpenings from "@/components/pages/collaboration/job_openings/JobOpenings";
import JobNominations from "@/components/pages/collaboration/job_openings/JobNominations";
import NomineeSelection from "@/components/pages/collaboration/job_openings/NomineeSelection";
import Questionnaires from "@/components/pages/collaboration/job_openings/Questionnaires";
import WeeklyMeetings from "@/components/pages/collaboration/WeeklyMeetings/";
import PhdInfo from "@/components/pages/collaboration/PhdInfo/";

export default [
  {
    path: "/collaboration/units/:unitType?/:unitDomain?",
    alias: ["/cms/units/:unitType?/:unitDomain?"],
    name: "units",
    component: OrgUnits,
    props(route) {
      return {
        unitType: decodeURI(route.params.unitType || ""),
        unitDomain: decodeURI(route.params.unitDomain || ""),
      };
    },
  },
  {
    path: "/collaboration/positions",
    alias: ["/cms/positions"],
    name: "org-positions",
    component: OrgPositions,
  },
  {
    path: "/collaboration/positions/ex-officio",
    alias: ["/cms/positions/ex-officio"],
    name: "exofficio-mandates",
    component: ExOfficioMandates,
  },
  {
    path: "/collaboration/institutes",
    alias: ["/cms/institutes"],
    name: "institutes",
    component: OrgInstitutes,
  },
  {
    path: "/collaboration/people",
    alias: ["/people"],
    name: "people",
    component: OrgPeople,
  },
  {
    path: "/authors",
    alias: ["/data/cmsAuthors"],
    name: "authors-list",
    component: OrgPeople,
    props: {
      filterStatus: [],
      filterAuthor: [true],
    },
  },
  {
    path: "/collaboration/cms-weeks/bookings/:weekId?",
    alias: ["/gm/cms_week_bookings/:weekId?"],
    name: "bookings",
    component: CmsBookings,
  },
  {
    path: "/collaboration/cms-weeks/weeks",
    alias: ["/gm/cms_weeks"],
    name: "weeks",
    component: CmsWeeks,
  },
  {
    path: "/collaboration/cms-weeks/rooms",
    alias: ["/gm/cms_week_rooms"],
    name: "rooms",
    component: CmsRooms,
  },
  {
    path: "/collaboration/voting/:code?",
    alias: ["/voting/votingList/:code?", "/voting/new", "/voting/list"],
    name: "voting",
    component: VotingList,
  },
  {
    path: "/collaboration/mo-list/:year?",
    alias: ["/gm/mo/:year?"],
    name: "mo-list",
    component: MoList,
    props: (route) => ({
      year: route.params.year || String(new Date().getFullYear()),
      mode: route.query.mode,
    }),
  },
  {
    path: "/collaboration/applicants",
    alias: ["/applicants", "/data/authorApplications"],
    name: "applicants",
    component: AuthorApplicants,
  },
  {
    path: "/collaboration/emeritus-nominations/:year?",
    alias: ["/cms/emeriti/:year?"],
    name: "emeritus-nominations",
    component: EmeritusNominations,
  },
  {
    path: "/collaboration/statistics",
    alias: ["/cms/stats"],
    name: "statistics",
    component: MemberStatistics,
  },
  {
    path: "/collaboration/flags",
    alias: ["/flags", "/data/flags"],
    name: "flags",
    component: DataFlags,
  },
  {
    path: "/collaboration/tenures",
    alias: ["/cms/tenures"],
    name: "tenures",
    component: CmsTenures,
  },
  {
    path: "/collaboration/communications",
    alias: ["/cms/comms"],
    name: "communications",
    component: CommunicationsOverview,
  },
  {
    path: "/collaboration/awards/overview",
    name: "awards",
    component: Awards,
  },
  {
    path: "/collaboration/awards/awardees",
    name: "awardees",
    component: Awardees,
  },
  {
    path: "/collaboration/awards/nomination-review-selection",
    name: "award-nomination-review-selection",
    component: AwardNominationReviewSelection,
  },
  {
    path: "/collaboration/awards/nomination-submission",
    alias: ["/collaboration/awards/nominations"],
    name: "award-nomination-submission",
    component: AwardNominations,
  },
  {
    path: "/collaboration/job-openings",
    name: "job-openings",
    component: JobOpenings,
  },
  {
    path: "/collaboration/job-nominations",
    name: "job-nominations",
    component: JobNominations,
  },
  {
    path: "/collaboration/nominee-selection",
    name: "nominee-selection",
    component: NomineeSelection,
  },
  {
    path: "/collaboration/questionnaires",
    name: "questionnaires",
    component: Questionnaires,
  },
  {
    path: "/collaboration/weekly-meetings",
    name: "weekly-meetings",
    component: WeeklyMeetings,
  },
  {
    path: "/collaboration/phd-info",
    name: "phd-info",
    component: PhdInfo,
  },
];
