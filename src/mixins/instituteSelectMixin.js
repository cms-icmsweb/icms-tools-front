import { uniqueBy } from "@/utils/helpers";

export default {
  data() {
    return {
      institute: null,
      instituteOptions: [],
    };
  },
  computed: {
    instituteSelectIsVisible() {
      return this.instituteOptions.length > 1;
    },
  },
  methods: {
    createInstituteOptions(data) {
      this.instituteOptions = uniqueBy(data, (row) => row.instCode)
        .sort((a, b) => {
          if (a.instCode < b.instCode) {
            return -1;
          }
          if (a.instCode > b.instCode) {
            return 1;
          }
          return 0;
        })
        .map((row) => ({ value: row.instCode, text: row.instCode }));
    },
  },
};
