module.exports = {
  root: true,

  env: {
    node: true,
    "jest/globals": true,
  },

  extends: ["plugin:vue/essential", "eslint:recommended"],

  parserOptions: {
    parser: "babel-eslint",
  },

  plugins: ["vue", "vuetify", "jest"],

  rules: {
    "vuetify/no-deprecated-classes": "error",
    "vuetify/grid-unknown-attributes": "error",
    "vuetify/no-legacy-grid": "error",

    "jest/no-disabled-tests": "warn",
    "jest/no-focused-tests": "error",
    "jest/no-identical-title": "error",
    "jest/prefer-to-have-length": "warn",
    "jest/valid-expect": "error",

    "no-unused-vars": "warn",
  },

  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)'
      ],
      env: {
        jest: true
      }
    }
  ]
};
